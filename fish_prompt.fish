
function __Y
  body
end
function fish_prompt --description 'Write out the prompt'

    # Just calculate these once, to save a few cycles when displaying the prompt
    if not set -q __fish_prompt_hostname
        set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
    end

    if not set -q __fish_prompt_normal
        set -g __fish_prompt_normal (set_color normal)
    end

    #if not set -q GIT_CONTEXT_PROMPT
        set GIT_BRANCH (git branch ^/dev/null | grep \* | sed 's/* //')
        set GIT_AHEAD (git_ahead)
        set GIT_MOD (git_porcelain -C)
        # ✓ ◦ ○
        set GIT_ORIGIN_STATUS_SYMBOL ✓
            set GIT_COLOR_BG green
            set GIT_COLOR black

        if not test -z $GIT_BRANCH
            if not test -z "$GIT_AHEAD"
                set GIT_ORIGIN_STATUS_SYMBOL $GIT_AHEAD
            end

            if not test -z "$GIT_MOD"
                if echo "$GIT_MOD" | grep -q -E '^\s*\d+U\s*$'
                    set GIT_COLOR_BG green
                    set GIT_COLOR black
                else
                    set GIT_COLOR_BG yellow
                    set GIT_COLOR black
                end
                # alternative "delta symbols" » ∆
                # [+ devfix-reset-modules (1A 10M)  ⚡︎]
                #set GIT_CONTEXT_PROMPT " "(set_color -b $GIT_COLOR_BG)(set_color $GIT_COLOR;)" $GIT_ORIGIN_STATUS_SYMBOL $GIT_BRANCH"(set_color normal; set_color -b $GIT_COLOR_BG; set_color black)" ($GIT_MOD) "(set_color normal;set_color -b black; set_color $GIT_COLOR_BG)" ⚡︎ "(set_color normal)" "

                # [+ devfix-reset-modules ∆ 1A 10M ⚡︎︎]
                set GIT_CONTEXT_PROMPT " "(set_color -b $GIT_COLOR_BG)(set_color $GIT_COLOR;)" $GIT_ORIGIN_STATUS_SYMBOL $GIT_BRANCH "(set_color -b black; set_color $GIT_COLOR_BG)" ∆ $GIT_MOD"(set_color normal;set_color -b black; set_color $GIT_COLOR_BG)" ⚡︎ "(set_color normal)" "
            else
                set GIT_CONTEXT_PROMPT " "(set_color -b $GIT_COLOR_BG)(set_color $GIT_COLOR)" $GIT_ORIGIN_STATUS_SYMBOL $GIT_BRANCH "(set_color -b black; set_color $GIT_COLOR_BG)" ⚡︎ "(set_color normal)" "
            end

            # set the color for the path to match that of the curren git
            set_color $GIT_COLOR_BG
            echo -n -s "" ' ' "$__fish_prompt_cwd" (prompt_pwd) "$__fish_prompt_normal"(set_color normal)
        else
            echo -n -s "" ' ' "$__fish_prompt_cwd" (prompt_pwd) "$__fish_prompt_normal"
        end
    #end
    #echo -n -s "" ' ' "$__fish_prompt_cwd" (prompt_pwd) "$__fish_prompt_normal"
    if test $GIT_CONTEXT_PROMPT; echo -n -s $GIT_CONTEXT_PROMPT; else; echo -n -s " "(set_color -b green)(set_color black)" ⚡︎ "(set_color normal)" "; end;
end
